﻿# Ajustes Maquina

## Parametros fisicos
- X (Anchura): 190mm
- Y(Profundidad): 180 mm
- Z(Altura): 135 mm

## Extrude
- Tamaño de la tobera: 0.4 mm
- Diametro del materal: 1.75mm


## Gcode de inicio con BL_Touch
G21 ;metric values
G90 ;absolute positioning
M82 ;set extruder to absolute mode
M107 ;start with the fan off
G28  ;move X/Y/Z to min endstops
G29; AutoLeveling
G92 Z12.45; set the z offset: 11.35+Z_offset
G1 Z0 F5000 ;move the platform down 15mm; G1 Z15.0
G92 E0 ;zero the extruded length
G1 F200 E3 ;extrude 3mm of feed stock
G92 E0 ;zero the extruded length again
G1 F9000
M117 Printing...


## Gcode Final
M104 S0 ;extruder heater off
M140 S0 ;heated bed heater off (if you have it)
G91 ;relative positioning
G1 E-1 F300  ;retract the filament a bit before lifting the nozzle, to release some of the pressure
G1 Z+0.5 E-5 X-20 Y-20 F9000 ;move Z up a bit and retract filament even more
G28 X0 Y0 ;move X/Y to min end stops, so the head is out of the way
M84 ;steppers off
G90 ;absolute positioning
