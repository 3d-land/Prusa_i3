# Images
![3D_freecad](/Model3D/EndStops/EndStop X/EndStop_X_freecad.png)
![photo_1](/Model3D/EndStops/EndStop X/EndStop_X_1.jpg)
![photo_1](/Model3D/EndStops/EndStop X/EndStop_X_2.jpg)

# License
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
============
Endstop Y Support for Prusa i3 licensed under the Creative Commons - Attribution license.

See commit details to find the authors of each Part.
- @fandres323
